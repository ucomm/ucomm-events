<?php

if ($eventsQuery->have_posts()) {
  while ($eventsQuery->have_posts()) {
    $eventsQuery->the_post();
    include(UC_EVENTS_PLUGIN_DIR . '/partials/public/event-item.php');
  }
  wp_reset_query();
}