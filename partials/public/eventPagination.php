<?php

$meta = get_post_meta(get_the_ID(), '_ucomm_event_date', true);

$prevQuery = new WP_Query([
  'post_status' => 'publish',
  'post_type' => 'ucomm-event',
  'posts_per_page' => 1,
  'meta_key' => '_ucomm_event_date',
  'meta_value' => [ '1970-01-01', $meta ],
  'meta_compare' => 'BETWEEN',
  // 'meta_value' => $meta,
  // 'meta_compare' => '<=',
  'meta_type' => 'DATE',
  'orderby' => 'meta_value',
  // 'offset' => 1
]);

$nextQuery = new WP_Query([
  'post_status' => 'publish',
  'post_type' => 'ucomm-event',
  'posts_per_page' => 1,
  'meta_key' => '_ucomm_event_date',
  'meta_value' => $meta,
  'meta_compare' => '>=',
  'meta_type' => 'DATE',
  'orderby' => 'meta_value',
  'offset' => 1
]);


?>



<div class="pagination-wrapper">
  <div class="prev-container">
    <?php
    if ($prevQuery->have_posts() && $prevQuery->posts[0]->ID !== $id) {
      while ($prevQuery->have_posts()) {
        echo "<div>previous</div>";
        $prevQuery->the_post();
        echo "<div><a href='" . get_the_permalink() . "'>" . get_the_title() . "</a></div>";
        // the_title();
      }
      wp_reset_query();
    }
    ?>
  </div>
  <div class="return-container">
    <a href="/events">All Events</a>
  </div>
  <div class="next-container">
    <?php
    if ($nextQuery->have_posts() && $nextQuery->posts[0]->ID !== $id) {
      while ($nextQuery->have_posts()) {
        echo "<div>next</div>";
        $nextQuery->the_post();
        echo "<div><a href='" . get_the_permalink() . "'>" . get_the_title() . "</a></div>";
        // the_title();
      }
      wp_reset_query();
    }
    ?>
  </div>
</div>