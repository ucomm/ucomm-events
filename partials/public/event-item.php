<?php

use UCommEvents\Frontend\Display;

$isEventPage = is_single() && 'ucomm-event' === get_post_type() ? true : false;

$id = get_the_ID();

?>

<article id="event-<?php echo $id; ?>" <?php post_class(); ?>>
  <section>
    <?php
    Display::getDatePartial($id);
    ?>
  </section>
  <section>
    <?php
      $titleOpenTag = '<' . $titleMarkup . '>';
      $titelCloseTag = '';

      if ($hasTitleLink) {
        $permalink = get_the_permalink();
        $titleOpenTag .= '<a href="'. $permalink . '">';
        $titleCloseTag ='</a></' . $titleMarkup . '>';
      } else {
        $titleCloseTag .= '</' . $titleMarkup . '>';
      }

      the_title($titleOpenTag, $titleCloseTag);
    ?>
  </section>
  <section class="event-content-wrapper">
    <?php 
      if ($showFeaturedImages || $isEventPage) {
    ?>
      <div class="featured-image-container">
        <?php
        if (has_post_thumbnail()) {
          the_post_thumbnail();
        } else {
          echo "<img alt='' class='thumbnail-fallback wp-post-image' src='" . UC_EVENTS_PLUGIN_URL . '/img/uconn-placeholder-3-2.jpg' . "' />";
        }
        ?>
      </div>
    <?php
      }
    ?>
    <div class="content-container">
      <?php
        if ($showLocations || $isEventPage) {
      ?>
        <div>
          <p>
            <?php the_terms($id, 'event-location', '', ', '); ?>
          </p>
        </div>
      <?php
        }
      ?>
      <?php
        if ($showDescriptions || $isEventPage) {
      ?>
        <div>
          <?php the_content(); ?>
        </div>
      <?php
        }
        if ($isEventPage) {
      ?>
        <div>
          <p>
            <a class="ucomm-event-link" href="<?php echo get_bloginfo('url'); ?>/events">All Events</a>
          </p>
        </div>
      <?php
        }
      ?>
    </div>
  </section>
</article>