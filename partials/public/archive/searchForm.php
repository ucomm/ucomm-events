<form id="events-search" method="GET" action="/events">
  <p>Search events by:</p>
  <div class="events-form-wrapper">
    <div class="events-input-wrapper">
      <div class="events-container events-label-container">
        <label for="events-text">
          Text
        </label>
      </div>
      <div class="events-container events-input-container">
        <input type="text" name="events-search[text]" id="events-text" placeholder="Monthly meeting" value="<?php echo $searchTextValue; ?>">
      </div>
    </div>
    <div class="events-input-wrapper">
      <div class="events-container events-label-container">
        <label for="events-year">Year</label>
      </div>
      <div class="events-container events-input-container">
        <input type="number" name="events-search[year]" id="events-year" min="2001" max="2099" placeholder="<?php echo date('Y'); ?>" value="<?php echo $yearValue; ?>">
      </div>
    </div>
    <div class="events-input-wrapper">
      <div class="events-container events-label-container">
        <label for="events-location">Location</label>
      </div>
      <div class="events-container events-input-container">
        <select name="events-search[location]" id="events-location">
          <option value="">Select a location</option>
          <?php
          foreach ($sortedLocations as $loc) {
          ?>
            <option value="<?php echo $loc->slug; ?>" <?php selected($_GET['events-search']['location'], $loc->slug); ?>>
              <?php
              echo $loc->name;
              if (count($loc->children) > 0) {
                foreach ($loc->children as $child) {
              ?>
            <option value="<?php echo $child->slug; ?>" <?php selected($_GET['events-search']['location'], $child->slug); ?>>-- <?php echo $child->name; ?></option>
        <?php
                }
              }
        ?>
        </option>
      <?php
          }
      ?>
        </select>
      </div>
    </div>
    <div class="events-input-wrapper">
      <div class="events-container events-submit-container">
        <input type="submit" value="Search">
      </div>
    </div>
  </div>
</form>