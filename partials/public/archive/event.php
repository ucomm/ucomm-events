<?php

use UCommEvents\Frontend\Display;

$id = get_the_ID();
$excerpt = get_the_excerpt();

?>

<div id="event-<?php echo $id; ?>" <?php post_class('ucomm-event ucomm-event-container') ?>>
  <?php
    Display::getDatePartial($id);
  ?>
  <div class="ucomm-event-detail-container">
    <div class="ucomm-event-title-container">
      <h2 class="ucomm-event-title">
        <span><?php the_title(); ?></span>
      </h2>
    </div>
    <div class="ucomm-event-description-container">
      <?php the_terms($id, 'event-location', '', ', '); ?>
      <p class="ucomm-event-description"><?php echo $excerpt; ?></p>
    </div>
    <div class="ucomm-event-link-container">
      <a class="ucomm-event-link" href="<?php the_permalink(); ?>" aria-label="Learn more about <?php the_title(); ?>">Learn More</a>
    </div>
  </div>
</div>