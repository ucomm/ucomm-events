<div class="pagination-wrapper">
  <div class="prev-container">
    <?php
      if (get_previous_posts_link()) {
        previous_posts_link('Previous');
      }
    ?>
  </div>
  <div class="next-container">
    <?php
      if (get_next_posts_link()) {
        next_posts_link('Next');
      }
    ?>
  </div>
</div>