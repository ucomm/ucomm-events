  <div class="ucomm-event-date-wrapper">
    <div class="ucomm-event-date-container ucomm-event-date-start-container">
      <p class="ucomm-event-date ucomm-event-start-date">
        <abbr title="<?php echo $start['fullMonth']; ?>"><?php echo $start['month']; ?></abbr>
        <span><?php echo $start['day']; ?></span>
      </p>
    </div>

    <div class="ucomm-event-date-container ucomm-event-date-end-container">
      <?php
      if (!$isSingleDate) {
      ?>
        <p class="ucomm-event-date ucomm-event-end-date">
          <abbr title="<?php echo $end['fullMonth']; ?>"><?php echo $end['month']; ?></abbr>
          <span><?php echo $end['day']; ?></span>
        </p>
      <?php
      }
      ?>
    </div>
  </div>