<div class="wrap">
  <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
  <div>
    <form id="ucomm-events-settings-form" action="options.php" method="POST">
      <?php
      settings_fields('ucomm-events-settings');

      do_settings_sections('ucomm-events-settings');
      ?>
      <div>
        <?php submit_button('Update Settings'); ?>
      </div>
    </form>
  </div>
</div>