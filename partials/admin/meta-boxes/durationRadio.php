<div>
  <div>
    <label for="<?php echo $metaArgs['args']['inputID']; ?>-single"><input type="radio" name="<?php echo $metaArgs['args']['inputName']; ?>" id="<?php echo $metaArgs['args']['inputID']; ?>-single" value="single" <?php checked($metaArgs['args']['value'], 'single'); ?>> Single Day</label>

  </div>
  <div>
    <label for="<?php echo $metaArgs['args']['inputID']; ?>-multiple"><input type="radio" name="<?php echo $metaArgs['args']['inputName']; ?>" id="<?php echo $metaArgs['args']['inputID']; ?>-multiple" value="multiple" <?php checked($metaArgs['args']['value'], 'multiple'); ?>> Multiple Days</label>

  </div>
</div>