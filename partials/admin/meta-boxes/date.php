<div class="events-meta-wrapper">
  <p><?php echo $metaArgs['args']['description']; ?></p>
  <label for='<?php echo $metaArgs['args']['inputID']; ?>'><?php echo $metaArgs['args']['inputLabel']; ?></label>
  <input type='date' name='<?php echo $metaArgs['args']['inputName']; ?>' id='<?php echo $metaArgs['args']['inputID']; ?>' class='events-meta-input events-meta-date' value="<?php echo $metaArgs['args']['value'] ?>" />
</div>