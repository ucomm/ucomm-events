<div class="container">
  <div>
    <label for="ucomm-events-route">Events page route</label>
  </div>
  <div>
    <input 
      id="ucomm-events-route" 
      type="text" 
      name="<?php echo $this->settingsName; ?>[ucomm-events-route]" 
      value="<?php echo $settings['ucomm-events-route']; ?>"
    >
    <p><strong>Only update this setting if you're sure there are no other routes using it.</strong></p>
  </div>
</div>