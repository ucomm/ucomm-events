const durationContainer = document.getElementById('ucomm-events-duration')
const singleDateRadio = document.getElementById('ucomm-event-duration-single')
const multiDateRadio = document.getElementById('ucomm-event-duration-multiple')
const eventDatePicker = document.getElementById('ucomm-event-date')
const eventEndDatePicker = document.getElementById('ucomm-event-end-date')

let eventDate = new Date(eventDatePicker.value)
let eventEndDate = new Date(eventEndDatePicker.value)

document.addEventListener('DOMContentLoaded', () => {
  eventDatePicker.setAttribute('max', eventEndDatePicker.value)
  eventEndDatePicker.setAttribute('min', eventDatePicker.value)
  if (singleDateRadio.checked) {
    eventEndDatePicker.setAttribute('readonly', 'readonly')
  }
})

durationContainer.addEventListener('click', (evt) => {
  if (evt.target.localName !== 'input') {
    return
  }

  if (singleDateRadio.checked) {
    eventEndDatePicker.value = eventDatePicker.value
    eventEndDatePicker.setAttribute('readonly', 'readonly')
    eventEndDatePicker.removeAttribute('min')
    eventDatePicker.removeAttribute('max')
  } else {
    eventEndDatePicker.removeAttribute('readonly')
  }

})

eventDatePicker.addEventListener('change', (evt) => {
  if (singleDateRadio.checked) {
    eventEndDatePicker.value = evt.target.value
  }

  eventEndDatePicker.setAttribute('min', evt.target.value)

})

eventEndDatePicker.addEventListener('change', (evt) => {
  if (singleDateRadio.checked) {
    return
  }

  eventDatePicker.setAttribute('max', evt.target.value)
})
