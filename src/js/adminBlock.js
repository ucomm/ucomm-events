import { registerBlockType } from '@wordpress/blocks'
import { eventsBlockName, eventsBlockData } from './blocks/ucomm-events-block'

registerBlockType(eventsBlockName, eventsBlockData)