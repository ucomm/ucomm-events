import { SelectControl } from '@wordpress/components'

import { __ } from '@wordpress/i18n'

const LocationSelect = (props) => {
  const {
    id,
    locations,
    value,
    onChangeLocation,
  } = props

  const options = [
    { label: 'All Locations', value: '' },
  ]

  locations.forEach(location => {
    options.push({
      label: location.name,
      value: location.slug,
    })
  })

  return (
    <SelectControl
      id={id}
      label={__('Location Filter', 'ucomm')}
      value={value}
      options={options}
      onChange={onChangeLocation}
    />
  )
}

export default LocationSelect