import { SelectControl } from '@wordpress/components'

import { __ } from '@wordpress/i18n'

const TitleMarkupSelect = (props) => {
  const {
    value,
    onChangeMarkup
  } = props

  const options = [
    { label: 'Plain Text', value: 'p' },
    { label: 'Heading 2', value: 'h2' },
    { label: 'Heading 3', value: 'h3' }
  ]

  return (
    <SelectControl 
      label={__('Title Markup')}
      value={value}
      options={options}
      onChange={onChangeMarkup}
    />
  )
}

export default TitleMarkupSelect