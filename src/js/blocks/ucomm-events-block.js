import { 
  useBlockProps,
  InspectorControls
} from '@wordpress/block-editor'

import {
  __experimentalNumberControl as NumberControl,
  __experimentalSpacer as Spacer,
  PanelBody,
  TextControl,
  ToggleControl,
} from '@wordpress/components'

import ServerSideRender from '@wordpress/server-side-render'

import { __ } from '@wordpress/i18n'

import { useSelect } from '@wordpress/data'

import LocationSelect from '../Components/LocationSelect'
import TitleMarkupSelect from '../Components/TitleMarkupSelect'

const eventsBlockJson = require('../../../block-config/ucomm-events.json')

export const eventsBlockName = eventsBlockJson.name

export const eventsBlockData = {
  apiVersion: eventsBlockJson.apiVersion,
  title: eventsBlockJson.title,
  icon: eventsBlockJson.icon,
  category: eventsBlockJson.category,
  attributes: eventsBlockJson.attributes,
  edit: (props) => {
    const { 
      attributes: {
        numEvents,
        location,
        keyword,
        offset,
        titleMarkup,
        hasTitleLink,
        showFeaturedImages,
        showDescriptions,
        showLocations
      }, 
      setAttributes 
    } = props

    const locations = useSelect(select => select('core').getEntityRecords('taxonomy', 'event-location', { per_page: -1 }), [])

    return (
      <div>
        <InspectorControls>
          <PanelBody title={ __('Number of Events') }>
            <NumberControl
              id="num-events-input"
              label={ __('Maximum Number of Events') } 
              onChange={ (value) => setAttributes({ 
                  numEvents: parseInt(value) 
                }) 
              }
              value={ numEvents }
            />
            <Spacer marginY={4} />
            <NumberControl 
              id="offset-input"
              label={ __('Event Offset') }
              onChange={ (value) => setAttributes({
                  offset: parseInt(value)
              })}
              value={ offset }
            />
          </PanelBody>
          <PanelBody title={ __('Filters') }>
            <p>
              <strong>NB - Filters are additive.</strong>
            </p>
            {
              !locations ? (
                <p>Loading locations...</p>
              ) : (
                <LocationSelect 
                  id="location-input"
                  locations={locations}
                  onChangeLocation={(value) => setAttributes({ location: value })}
                  value={location}
                />
              )
            }
            <TextControl 
              id="keyword-input"
              label="Keyword Search Filter"
              value={keyword}
              onChange={ (value) => setAttributes({ keyword: value }) }
            />
          </PanelBody>
          <PanelBody title={__('Display Options')}>
            <TitleMarkupSelect 
              value={titleMarkup}
              onChangeMarkup={(value) => setAttributes({ titleMarkup: value })}
            />
            <ToggleControl
              className="title-link-input"
              label={__('Use Title Link')}
              help={
                `${hasTitleLink ? 'Enable' : 'Disable'} links in the title for each event.`
              }
              checked={hasTitleLink}
              onChange={(value) => setAttributes({ hasTitleLink: value })}
            />
            <ToggleControl 
              className="show-image-input"
              label={__('Featured Images')}
              help={
                `${showFeaturedImages ? 'Show' : 'Hide'} featured images for each event. A safe fallback is provided for events without an image.`
              }
              checked={ showFeaturedImages }
              onChange={(value) => setAttributes({ showFeaturedImages: value })}
            />
            <ToggleControl
              className="show-descriptions-input"
              label={__('Event Descriptions')}
              help={
                `${showDescriptions ? 'Show' : 'Hide'} descriptions for each event.`
              }
              checked={showDescriptions}
              onChange={(value) => console.log({ value }) || setAttributes({ showDescriptions: value })}
            />
            <ToggleControl
              className="show-locations-input"
              label={__('Event Locations')}
              help={
                `${showLocations ? 'Show' : 'Hide'} locations for each event.`
              }
              checked={showLocations}
              onChange={(value) => setAttributes({ showLocations: value })}
            />
          </PanelBody>
        </InspectorControls>
        <div {...useBlockProps()}>
          <div>
            <ServerSideRender
              block={eventsBlockName}
              attributes={{
                numEvents,
                location,
                keyword,
                offset,
                titleMarkup,
                hasTitleLink,
                showFeaturedImages,
                showDescriptions,
                showLocations
              }}
              LoadingResponsePlaceholder={() => (
                  <p>{ __(`Loading events...`) }</p>
                )
              }
            />
          </div>
        </div>
      </div>
    )
  },
  save: () => { return null }
}