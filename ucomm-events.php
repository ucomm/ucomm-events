<?php
/*
Plugin Name: UComm Events
Description: Create and display custom event posts
Author: UComm Web Team
Version: 0.0.1
Text Domain: ucomm-events
*/

// rename this file according to the plugin.

use UCommEvents\Assets\ScriptLoader;
use UCommEvents\Assets\StyleLoader;
use UCommEvents\Database\Database;
use UCommEvents\Frontend\Block;
use UCommEvents\Frontend\Display;
use UCommEvents\Frontend\Shortcode;
use UCommEvents\PostTypes\Events;
use UCommEvents\Taxonomies\Locations;
use UCommEvents\MetaBoxes\EventDate;
use UCommEvents\Endpoints\EventsRESTQuery;

if (!defined('WPINC')) {
	die;
}

define( 'UC_EVENTS_PLUGIN_DIR', plugin_dir_path(__FILE__) );
define( 'UC_EVENTS_PLUGIN_URL', plugins_url('/', __FILE__) );

require('lib/Assets/Loader.php');
require('lib/Assets/ScriptLoader.php');
require('lib/Assets/StyleLoader.php');
require('lib/Database/Database.php');
require('lib/Frontend/Block.php');
require('lib/Frontend/Display.php');
require('lib/Frontend/Shortcode.php');
require('lib/PostTypes/Events.php');
require('lib/Taxonomies/Locations.php');
require('lib/MetaBoxes/EventDate.php');
require('lib/Endpoints/Events.php');

$sciptLoader = new ScriptLoader();
$styleLoader = new StyleLoader();
$display = new Display();
$database = new Database();

if (!is_admin()) {
	
	$shortcode = new Shortcode($display);
	$shortcode->addShortcode();
	$sciptLoader->enqueueAssets();
	$styleLoader->enqueueAssets();

	$display->filterTemplates();
	$database->handlePreGetPosts();
} else {
	$sciptLoader->enqueueAdminAssets();
	$styleLoader->enqueueAdminAssets();	
}

$ucommEvents = new Events();
$ucommEvents->createPostType();

$locationTaxonomy = new Locations();
$locationTaxonomy->createTaxonomy();

$block = new Block($sciptLoader);
$block->init();

add_action('add_meta_boxes', [ EventDate::class, 'addDateMetaBoxes' ]);
add_action('save_post', [ EventDate::class, 'saveDateMetaBox']);

add_filter('rest_ucomm-event_query', [ EventsRESTQuery::class, 'eventsQuery' ], 10, 2);
