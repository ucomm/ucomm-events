<?php

use UCommEvents\Taxonomies\Locations;

get_header(); 

?>

<div>
  <?php

  if (have_posts()) {

    $sortedLocations = [];

    $locations = get_terms([
      'taxonomy' => 'event-location',
      'hide_empty' => false
    ]);

    Locations::sortLocations($locations, $sortedLocations, 0);

    $isSearch = isset($_GET['events-search']);
    $isTextSearch = $isSearch && !empty($_GET['events-search']['text']);
    $isYearSearch = $isSearch && !empty($_GET['events-search']['year']);
    $searchTextValue = $isTextSearch ? $_GET['events-search']['text'] : '';
    $yearValue = $isYearSearch ? $_GET['events-search']['year'] : '';

    include(UC_EVENTS_PLUGIN_DIR . '/partials/public/archive/searchForm.php');
    while (have_posts()) {
      the_post();
      include(UC_EVENTS_PLUGIN_DIR . '/partials/public/archive/event.php');
    }

    include(UC_EVENTS_PLUGIN_DIR . '/partials/public/archive/pagination.php');
  }

  ?>
</div>

<?php get_footer(); ?>