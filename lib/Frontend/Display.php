<?php

namespace UCommEvents\Frontend;

/**
 *
 *  Handle display functionality for the plugin such as
 * - template fetching
 * - filtering query results
 *
 */
class Display {

  public function addShortCode() {
    $this->shortcode->addShortcode();
  }

  public function filterTemplates()
  {
    add_filter('single_template', [$this, 'getTemplate'], 10, 3);
    add_filter('archive_template', [$this, 'getTemplate'], 10, 3);
  }

  /**
   * Retrieve the correct template to display the event archive or post. This lets us choose different layout, styles, etc from whatever theme is being used
   *
   * @param string $template
   * @param string $type
   * @param array $templates
   * @return string
   */
  public static function getTemplate(string $template, string $type, array $templates) {
    global $post;

    if (is_archive() && 'ucomm-event' === $post->post_type) {
      return UC_EVENTS_PLUGIN_DIR . '/templates/ucomm-events-archive.php';
    } elseif (is_single() && 'ucomm-event' === $post->post_type) {
      return UC_EVENTS_PLUGIN_DIR . '/partials/public/event-page.php';
    }

    return $template;
  }

  public static function formatEventDate(int $postID, string $metaKey) {
    $rawDate = get_post_meta($postID, $metaKey, true);
    $date = strtotime($rawDate);
    return [
      'fullMonth' => date('F', $date),
      'month' => date('M', $date),
      'day' => date('d', $date)
    ];
  }

  public static function getDatePartial(int $postID) {
    $isSingleDate = 'single' === get_post_meta($postID, '_ucomm_event_duration', true) ? true : false;
    $start = self::formatEventDate($postID, '_ucomm_event_date');
    $end = [];
    if (!$isSingleDate) {
      $end = self::formatEventDate($postID, '_ucomm_event_end_date');
    }
    include(UC_EVENTS_PLUGIN_DIR . 'partials/public/eventDates.php');
  }
}