<?php

namespace UCommEvents\Frontend;

use UCommEvents\Database\Database;
use WP_Query;

class Shortcode {

  public $atts;
  public $display;

  private $db;

  public function __construct(Display $display)
  {
    $this->atts = [];
    $this->display = $display;
    $this->db = new Database();
  }

  public static $shortcodeSlug = 'ucomm-events';

  public function addShortcode() {
    add_shortcode(self::$shortcodeSlug, [ $this, 'display' ]);
  }

  public function display($atts = []) {
    $eventsQuery = $this->createQuery($atts);
    $showFeaturedImages = $atts['show-featured-images'];
    $showDescriptions = $atts['show-descriptions'];
    $showLocations = $atts['show-locations'];
    $titleMarkup = $this->validateTitleMarkup($atts['title-markup']);
    $hasTitleLink = $atts['has-title-link'];
    
    ob_start();
    include(UC_EVENTS_PLUGIN_DIR . 'partials/public/public-display.php');
    return ob_get_clean();
  }

  public static function getShortcodeSlug(): string {
    return self::$shortcodeSlug;
  }

  public function createQuery($atts = []): WP_Query {
    return $this->db->setQuery($this->setAtts($atts));
  }

  public function setAtts($atts): array {
    if (!is_array($atts)) {
      return [];
    }

    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    $this->atts = shortcode_atts(
      [
        'number-of-events' => 10,
        'location' => '',
        'keyword' => '',
        'offset' => 0,
        'show-featured-images' => false,
        'show-descriptions' => false,
        'show-locations' => false,
        'title-markup' => 'p',
        'has-title-link' => true
      ],
      $atts,
      self::$shortcodeSlug
    );
    return $this->atts;
  }

  public function validateTitleMarkup(string $tag): string {
    return 'p' === $tag || 'h2' === $tag || 'h3' === $tag ?
      $tag :
      'p';
  }

  public function validateBoolean($val): bool {
    return filter_var($val, FILTER_VALIDATE_BOOLEAN);
  }
}