<?php

namespace UCommEvents\Frontend;

use UCommEvents\Assets\ScriptLoader;

class Block {

  private $scriptLoader;

  public function __construct(ScriptLoader $scriptLoader) 
  {
    $this->scriptLoader = $scriptLoader;
  }

  public function init() {
    add_action('init', [ $this, 'registerBlockType' ]);
  }

  public function registerBlockType() {
    $this->scriptLoader->registerEventsBlockScript();
    $editorScript = $this->scriptLoader->getEditorScriptHandle();

    $config = file_get_contents(UC_EVENTS_PLUGIN_DIR . 'block-config/ucomm-events.json');
    $config = json_decode($config);

    register_block_type($config->name, [
      'editor_script' => $editorScript,
      'attributes' => json_decode(json_encode($config->attributes), true),
      'render_callback' => function($block_attributes, $content) {

        $display = new Display();
        $shortcode = new Shortcode($display);

        $numEvents = strval($block_attributes['numEvents']);
        $offset = strval($block_attributes['offset']);
        $location = strval($block_attributes['location']);
        $keyword = $block_attributes['keyword'];
        $titleMarkup = $shortcode->validateTitleMarkup($block_attributes['titleMarkup']);
        $hasTitleLink = $shortcode->validateBoolean($block_attributes['hasTitleLink']);
        $showFeaturedImages = $shortcode->validateBoolean($block_attributes['showFeaturedImages']);
        $showDescriptions = $shortcode->validateBoolean($block_attributes['showDescriptions']);
        $showLocations = $shortcode->validateBoolean($block_attributes['showLocations']);

        return $shortcode->display([
          'number-of-events' => $numEvents,
          'offset' => $offset,
          'location' => $location,
          'keyword' => $keyword,
          'title-markup' => $titleMarkup,
          'has-title-link' => $hasTitleLink,
          'show-featured-images' => $showFeaturedImages,
          'show-descriptions' => $showDescriptions,
          'show-locations' => $showLocations
        ]);

      }
    ]);
  }
}