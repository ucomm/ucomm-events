<?php

namespace UCommEvents\MetaBoxes;

class EventDate {
  public static function addDateMetaBoxes() {

    add_meta_box(
      'ucomm-events-duration',
      __('Event Duration', 'ucomm-events'),
      [self::class, 'renderDurationMetaBox'],
      'ucomm-event',
      'side',
      'default',
      [
        'inputID' => 'ucomm-event-duration',
        'inputName' => 'ucomm-event-meta[duration]',
        'inputLabel' => __('Toggle between single and multi-day events', 'ucomm-event'),
      ]
    );

    add_meta_box(
      'ucomm-events-date',
      __('Event Date', 'ucomm-events'),
      [ self::class, 'renderDateMetaBox' ],
      'ucomm-event',
      'side',
      'default',
      [
        'description' => '',
        'inputID' => 'ucomm-event-date',
        'inputName' => 'ucomm-event-meta[date]',
        'inputLabel' => __('Start Date', 'ucomm-events')
      ]
    );

    add_meta_box(
      'ucomm-events-end-date',
      __('Event End Date', 'ucomm-events'),
      [self::class, 'renderDateMetaBox'],
      'ucomm-event',
      'side',
      'default',
      [
        'description' => 'Leave this box empty for events which begin and end on the same date.',
        'inputID' => 'ucomm-event-end-date',
        'inputName' => 'ucomm-event-meta[end-date]',
        'inputLabel' => __('End Date', 'ucomm-events')
      ]
    );
  }

  public static function saveDateMetaBox(int $postID) {
    if (!array_key_exists('ucomm-event-meta', $_POST)) {
      return;
    }

    update_post_meta(
      $postID,
      '_ucomm_event_duration',
      $_POST['ucomm-event-meta']['duration']
    );

    update_post_meta(
      $postID,
      '_ucomm_event_date',
      $_POST['ucomm-event-meta']['date']
    );

    update_post_meta(
      $postID,
      '_ucomm_event_end_date',
      $_POST['ucomm-event-meta']['end-date']
    );
  }

  public function renderDateMetaBox($post, $metaArgs) {

    $value = '';
    $eventMetaValue = get_post_meta($post->ID, '_ucomm_event_date', true);
    $eventEndMetaValue = get_post_meta($post->ID, '_ucomm_event_end_date', true);

    $eventDate = '' !== $eventMetaValue ? $eventMetaValue : current_time('Y-m-d');
    $eventEnd = '' !== $eventEndMetaValue ? $eventEndMetaValue : current_time('Y-m-d');

    if ($metaArgs['args']['inputID'] === 'ucomm-event-date') {
      $value = $eventDate;
    } elseif ($metaArgs['args']['inputID'] === 'ucomm-event-end-date') {
      $value = $eventEnd;
    }

    $metaArgs['args']['value'] = $value;

    include UC_EVENTS_PLUGIN_DIR . '/partials/admin/meta-boxes/date.php';
  }

  public function renderDurationMetaBox($post, $metaArgs) {

    $durationMetaValue = get_post_meta($post->ID, '_ucomm_event_duration', true);
    $value = '' !== $durationMetaValue ? $durationMetaValue : 'single';

    $metaArgs['args']['value'] = $value;

    include UC_EVENTS_PLUGIN_DIR . '/partials/admin/meta-boxes/durationRadio.php';
  }

  public static function dateError() {
    echo "<div class='notice notice-error'>
      <p>You may not have an event end before it began.</p>
    </div>";
  }
}