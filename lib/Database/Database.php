<?php

namespace UCommEvents\Database;

use WP_Query;

/**
 * Prepare the database
 * - Keep track of the database version
 * - Allow access to the WP db functions without exposing them to the global scope
 * 
 * This class allows encapsulates the global WP database. That way it's not completely exposed at the global scope. It can also be extended so that you can use different classes to do things like:
 * - create and manage tables
 * - perform queries and inserts
 * - any other db operation... without cluttering this class too much.
 * 
 */
class Database
{

  private $queryParams;

  public function __construct()
  {
    $this->queryParams = [
      'post_status' => 'publish',
      'meta_query' => [
        'start_date_clause' => [
          'key' => '_ucomm_event_date',
          'compare' => 'EXISTS'
        ],
        'end_date_clause' => [
          'key' => '_ucomm_event_end_date',
          'value' => current_time('Y-m-d'),
          'compare' => '>=',
          'type' => 'DATE'
        ]
      ],
      'orderby' => [
        'start_date_clause' => 'ASC'
      ]
    ];
  }

  public function handlePreGetPosts() {
    add_filter('pre_get_posts', [$this, 'preGetPosts']);
  }

  /**
   * The archive page for event posts must display the posts according to the following rules
   * - Ascending chronological order based on start date
   * - Events in the past should not be displayed unless...
   * - the event has an _end_ date in the future
   * 
   * The query must also support
   * - keyword search
   * - search based on date (year)
   * - search based on taxonomy (location)
   *
   * @param WP_Query $query
   * @return WP_Query
   */
  public function preGetPosts(WP_Query $query): WP_Query
  {

    if ($query->is_archive() && 'ucomm-event' === $query->query['post_type']) {

      $isSearch = isset($_GET['events-search']) ? true : false;
      $isTextSearch = $isSearch && !empty($_GET['events-search']['text']) ? true : false;
      $isYearSearch = $isSearch && !empty($_GET['events-search']['year']) ? true : false;
      $isLocationSearch = $isSearch && !empty($_GET['events-search']['location']) ? true : false;

      /**
       * 
       * All events have a beginning date and ending date
       * For events that are only on one day, these dates match
       * That lets us check that no events ending in the past will be included
       * Because of the way the orderby clause works, we _have_ to check that the start date exists
       * Events are ordered according to that date
       * See - https://developer.wordpress.org/reference/classes/wp_query/#order-orderby-parameters
       * 
       */
      $toSet = $this->getQueryParams();

      if ($isTextSearch) {
        $toSet['s'] = $_GET['events-search']['text'];
      }

      if ($isYearSearch) {
        $toSet['meta_query']['end_date_clause']['value'] = $_GET['events-search']['year'] . '-01-01';
      }

      if ($isLocationSearch) {
        $toSet['tax_query'] = $this->setTaxQueryParams($_GET['events-search']['location']);
      }

      foreach ($toSet as $key => $value) {
        $query->set($key, $value);
      }
    }

    return $query;
  }

  /**
   * Takes in arguments from the shortcode and returns a new WP Query object
   *
   * @param array $atts
   * @return WP_Query
   */
  public function setQuery(array $atts = []): WP_Query
  {
    $args = $this->getQueryParams();
    $args['post_type'] = 'ucomm-event';


    if (10 !== $atts['number-of-events']) {
      $args['posts_per_page'] = $atts['number-of-events'];
    }

    if (0 !== $atts['offset']) {
      $args['offset'] = $atts['offset'];
    }

    if (null !== $atts['location'] && '' !== $atts['location']) {
      $args['tax_query'] = $this->setTaxQueryParams($atts['location']);
    }

    if ('' !== $atts['keyword']) {
      $args['s'] = $atts['keyword'];
    }

    return new WP_Query($args);
  } 

  /**
   * Prepare a taxonomy database query for posts
   *
   * @param string $location
   * @return array
   */
  public function setTaxQueryParams(string $location): array {
    return [
      [
        'taxonomy' => 'event-location',
        'field' => 'slug',
        'terms' => $location
      ]
    ];
  }

  public function getQueryParams() {
    return $this->queryParams;
  }
}