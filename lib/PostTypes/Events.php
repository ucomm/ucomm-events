<?php

namespace UCommEvents\PostTypes;

class Events {

  public function createPostType() {
    add_action('init', [ $this, 'registerPostType' ]);
  }


  public function registerOnActivation()
  {
    $this->registerPostType();
    flush_rewrite_rules(false);
  }

  public function registerPostType() {
    $labels = [
      'name'                  => _x('Events', 'Post Type General Name', 'ucomm-events'),
      'singular_name'         => _x('Event', 'Post Type Singular Name', 'ucomm-events'),
      'menu_name'             => __('Events', 'ucomm-events'),
      'name_admin_bar'        => __('Events', 'ucomm-events'),
      'archives'              => __('Event Archives', 'ucomm-events'),
      'attributes'            => __('Event Attributes', 'ucomm-events'),
      'parent_item_colon'     => __('Parent Event:', 'ucomm-events'),
      'all_items'             => __('All Events', 'ucomm-events'),
      'add_new_item'          => __('Add New Event', 'ucomm-events'),
      'add_new'               => __('Add New', 'ucomm-events'),
      'new_item'              => __('New Event', 'ucomm-events'),
      'edit_item'             => __('Edit Event', 'ucomm-events'),
      'update_item'           => __('Update Event', 'ucomm-events'),
      'view_item'             => __('View Event', 'ucomm-events'),
      'view_items'            => __('View Events', 'ucomm-events'),
      'search_items'          => __('Search Event', 'ucomm-events'),
      'not_found'             => __('Not found', 'ucomm-events'),
      'not_found_in_trash'    => __('Not found in Trash', 'ucomm-events'),
      'featured_image'        => __('Featured Image', 'ucomm-events'),
      'set_featured_image'    => __('Set featured image', 'ucomm-events'),
      'remove_featured_image' => __('Remove featured image', 'ucomm-events'),
      'use_featured_image'    => __('Use as featured image', 'ucomm-events'),
      'insert_into_item'      => __('Insert into item', 'ucomm-events'),
      'uploaded_to_this_item' => __('Uploaded to this item', 'ucomm-events'),
      'items_list'            => __('Events list', 'ucomm-events'),
      'items_list_navigation' => __('Events list navigation', 'ucomm-events'),
      'filter_items_list'     => __('Filter items list', 'ucomm-events'),
    ];

    $rewrite = [
      'slug' => 'events',
      'with_front' => true,
      'pages' => true,
      'feeds' => true
    ];

    $args = [
      'label'                 => __('Event', 'ucomm-events'),
      'description'           => __('Department Events', 'ucomm-events'),
      'labels'                => $labels,
      'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'excerpt' ],
      'taxonomies'            => [ 'category', 'post_tag' ],
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'menu_icon'             => 'dashicons-calendar',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => 'events',
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
      'rewrite'               => $rewrite,
      'show_in_rest'          => true,
      'rest_base'             => 'events'
    ];

    register_post_type('ucomm-event', $args);
  }
}