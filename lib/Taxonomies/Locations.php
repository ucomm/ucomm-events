<?php

namespace UCommEvents\Taxonomies;

class Locations {

  public function createTaxonomy() {
    add_action('init', [ $this, 'registerTaxonomy' ]);
  }

  public function registerTaxonomy() {
    $labels = [
      'name'                       => _x('Event Locations', 'Taxonomy General Name', 'text_domain'),
      'singular_name'              => _x('Event Location', 'Taxonomy Singular Name', 'text_domain'),
      'menu_name'                  => __('Locations', 'text_domain'),
      'all_items'                  => __('All Locations', 'text_domain'),
      'parent_item'                => __('Parent Location', 'text_domain'),
      'parent_item_colon'          => __('Parent Location:', 'text_domain'),
      'new_item_name'              => __('New Location Name', 'text_domain'),
      'add_new_item'               => __('Add New Location', 'text_domain'),
      'edit_item'                  => __('Edit Location', 'text_domain'),
      'update_item'                => __('Update Location', 'text_domain'),
      'view_item'                  => __('View Location', 'text_domain'),
      'separate_items_with_commas' => __('Separate locations with commas', 'text_domain'),
      'add_or_remove_items'        => __('Add or remove locations', 'text_domain'),
      'choose_from_most_used'      => __('Choose from the most used', 'text_domain'),
      'popular_items'              => __('Popular Locations', 'text_domain'),
      'search_items'               => __('Search Locations', 'text_domain'),
      'not_found'                  => __('Not Found', 'text_domain'),
      'no_terms'                   => __('No locations', 'text_domain'),
      'items_list'                 => __('Locations list', 'text_domain'),
      'items_list_navigation'      => __('Locations list navigation', 'text_domain'),
    ];
    $args = [
      'labels'                     => $labels,
      'hierarchical'               => true,
      'public'                     => true,
      'show_ui'                    => true,
      'show_admin_column'          => true,
      'show_in_nav_menus'          => true,
      'show_tagcloud'              => true,
      'show_in_rest'               => true,
      'rest_base'                  => 'event-locations',
    ];
    register_taxonomy('event-location', ['ucomm-event'], $args);
  }

  public static function sortLocations(array &$cats, array &$into, $parentID = 0) {
    foreach ($cats as $i => $cat) {
      if ($cat->parent == $parentID) {
        $into[] = $cat;
        unset($cats[$i]);
      }
    }

    foreach ($into as $topCat) {
      $topCat->children = array();
      self::sortLocations($cats, $topCat->children, $topCat->term_id);
    }
  }
}