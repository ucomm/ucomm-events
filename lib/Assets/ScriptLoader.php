<?php

namespace UCommEvents\Assets;

use UCommEvents\Assets\Loader;

/**
 * A class to handle loading JS assets
 */
class ScriptLoader extends Loader {
  private $editorScriptHandle;

  public function __construct()
  {
    $this->editorScriptHandle = $this->handle . '-block';
    parent::__construct();
  }
  /**
   * Enqueue the prepared scripts.
   * In this example, the script will only be enqueued if the shortcode is present.
   *
   * @return void
   */
  public function enqueue() {
    global $post;

    if ('ucomm-event' === $post->post_type) {
      $this->prepareAppScript();
      wp_enqueue_script($this->handle);
    }
  }

  /**
   * This method can be used to enqueue an asset on an admin page.
   * Use the slug to filter which pages it should be used on.
   *
   * @param string $hook - the admin page's slug to enqueue on
   * @return void
   */
  public function adminEnqueue(string $hook) {
    global $current_screen;
    if ('ucomm-event' === $current_screen->id) {
      $this->prepareAdminScript();
      wp_enqueue_script($this->handle . '-admin');
    }
  }

  public function getAssetFilePath() {
    return $this->assetFilePath;
  }

  public function getEditorScriptHandle() {
    return $this->editorScriptHandle;
  }

  /**
   * Prepare the script by registering it.
   *
   * @return void
   */
  private function prepareAppScript() {
    $scriptDeps = [];
    wp_register_script(
      $this->handle,
      UC_EVENTS_PLUGIN_URL . $this->buildDir . '/main.js',
      $scriptDeps,
      false,
      true
    );
  }

  public function registerEventsBlockScript() {
    $assetFile = include($this->assetFilePath); 
    return wp_register_script(
      $this->editorScriptHandle,
      UC_EVENTS_PLUGIN_URL . $this->buildDir . '/adminBlock.js',
      $assetFile['dependencies'],
      $assetFile['version'],
      true
    );
  }

  private function prepareAdminScript() {
    $scriptDeps = [];
    wp_register_script(
      $this->handle . '-admin',
      UC_EVENTS_PLUGIN_URL . $this->buildDir . '/admin.js',
      $scriptDeps,
      false,
      true
    );
  }
}