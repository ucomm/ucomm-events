<?php

namespace UCommEvents\Endpoints;

use WP_REST_Request;

class EventsRESTQuery {
  /**
   * Modifies the base WP REST API to include additional parameters on the standard post type.
   *
   * @param array $args query args to pass to the database
   * @param WP_REST_Request $request the REST request
   * @return array modified query args
   */
  public static function eventsQuery(array $args, WP_REST_Request $request): array {
    $params = $request->get_params();

    if (!empty($params['location'])) {
      $args['tax_query'] = [
        [
          'taxonomy' => 'event-location',
          'field' => 'slug',
          'terms' => $params['location']
        ]
      ];
    }

    return $args;
  }
}