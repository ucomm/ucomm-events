<?php

use UCommEvents\Taxonomies\Locations;

require dirname(__DIR__, 2) . '/lib/Taxonomies/Locations.php';

class CustomTaxCest
{
    public function _before(UnitTester $I)
    {
    }

    public function hasLocationTaxonomy(UnitTester $I)
    {
        $I->assertTrue(class_exists(UCommEvents\Taxonomies\Locations::class));

        $locationTaxonomy = new Locations();
        $I->assertIsObject($locationTaxonomy);
    }

    public function canRegisterLocationTaxonomy(UnitTester $I) 
    {
        $locationTaxonomy = new Locations();
        $I->assertTrue(method_exists($locationTaxonomy, 'registerTaxonomy'), 'Taxonomy not registered');
        $I->assertTrue(method_exists($locationTaxonomy, 'createTaxonomy'), 'Taxonomy not created');
    }
}
