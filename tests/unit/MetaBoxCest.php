<?php

require dirname(__DIR__, 2) . '/lib/MetaBoxes/EventDate.php';

class MetaBoxCest
{
    public function _before(UnitTester $I)
    {
    }

    public function hasDateMetaboxClass(UnitTester $I)
    {
        $I->assertTrue(class_exists(\UCommEvents\MetaBoxes\EventDate::class));
    }
}
