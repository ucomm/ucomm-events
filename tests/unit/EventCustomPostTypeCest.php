<?php

use UCommEvents\PostTypes\Events;

require dirname(__DIR__, 2) . '/lib/PostTypes/Events.php';

class EventCustomPostTypeCest
{
    public function _before(UnitTester $I)
    {
    }

    public function hasEventsPostTypeClass(UnitTester $I)
    {
        $I->assertTrue(class_exists(\UCommEvents\PostTypes\Events::class));

        $eventsClass = new Events();
        $I->assertIsObject($eventsClass);
    }

    public function eventClassHasMethods(UnitTester $I) {
        $message = 'Class does not have method ';
        $eventsClass = new Events();
        $I->assertTrue(method_exists($eventsClass, 'registerPostType'), $message . 'registerPostType');
        $I->assertTrue(method_exists($eventsClass, 'createPostType'), $message . 'createPostType');
    }
}
