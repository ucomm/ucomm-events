<?php

class EventPostTypeCest
{
    public function _before(FunctionalTester $I)
    {
    }

    public function haveAnEventPost(FunctionalTester $I)
    {
        $I->havePostInDatabase([
            'post_type' => 'ucomm-event',
            'post_title' => 'Test Event'
        ]);
    }
}
